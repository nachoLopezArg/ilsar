package com.ilsar.auth.repository;

import java.util.List;

import com.ilsar.auth.entities.UserRole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRolesRepository extends CrudRepository<UserRole, Long> {

    @Query("select a.role from com.ilsar.auth.entities.UserRole a, User b where b.userName=?1 and a.userid=b.userId")
    List<String> findRoleByUserName(String username);

}