package com.ilsar.models;

public class Header {
    private String title;
    private String description;
    private String logoLink;
    private String userName;
}
