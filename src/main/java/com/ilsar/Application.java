package com.ilsar;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;


@EnableWebSecurity
@SpringBootApplication
public class Application  extends SpringBootServletInitializer {

//    @Autowired
//    private UserRepository userRepository;

//@Autowired
//private UserDAO userDao;
//
//
//    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//
//    @Autowired
//    private UserRepository userRepository;
//
//    @PostConstruct
//    public void init(){
//        User user = new User(
//                "Memory",
//                "Not Found",
//                "info@memorynotfound.com",
//                passwordEncoder.encode("password"),
//                Arrays.asList(
//                        new Role("SITE_USER"),
//                        new Role("SITE_ADMIN")));
//
//        if (userRepository.findByEmail(user.getEmail()) == null){
//            userRepository.save(user);
//        }
//    }
    public static void main(String[] args) {

        SpringApplication.run(Application.class, args);
    }


//    @Bean
//    InitializingBean sendDatabase() {
//        return () -> {
//
//            this.prepararUsuarioMaster();
//        };
//    }

//    private void prepararUsuarioMaster() throws CantGoOnException{
//        try {
//
//            /*ENTIDAD*/
//            Entidad entidad = new Entidad("ITSMYTURN", "ITSMYTURN.com");
//            /*ROLES DE ADMIN*/
//            List<Role> rolesDeUsuario = new ArrayList<>();
//            Role siteAdminRol = new Role(Role.SITE_ROLES.SITE_ADMIN);
//            siteAdminRol.setEntidad(entidad);
//            rolesDeUsuario.add(siteAdminRol);
//           /*DATOS DEL USUARIO*/
//            User usuarioAdmin = new User("Admin", "itsmyturn", rolesDeUsuario);
//            usuarioAdmin.setEmail("igloar96@gmail.com");
//            usuarioAdmin.setEnabled(true);
//
//            this.userDao.save(usuarioAdmin);
//        }catch(Exception e){
//            e.printStackTrace();
//            throw new CantGoOnException(e);
//        }
//
//    }



}

