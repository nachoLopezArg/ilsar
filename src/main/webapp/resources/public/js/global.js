// HOST CONFIG-------------------------------------
DOMAIN='http://localhost/turnos';
LAYOUT=DOMAIN + '/layouts/BOX_DEFAULT/';
//-------------------------------------------------

// Imports ------------------------------------------------------------------------
//$.getScript(DOMAIN+"/middleware/js/jquery.cookie.js", null);
//---------------------------------------------------------------------------------

// ONLOAD ABAJO DE TODO
//->
mkGetRest = function(to,succed,error){
    $.ajax({
        url: DOMAIN+"/middleware/dbConn/connect.php?to="+to,
        //dataType: "json",
        type:"GET",
        processData: false,  // tell jQuery not to process the data
        contentType: false,   // tell jQuery not to set contentType
    })
        .done(succed)
        .fail(function(){if(error==undefined){notification_red(e.responseText )}else{error}});
    //puedo mandar una funcion para error tambien !
};
mkPostRest = function(id_form){
    //Obtengo todos los campos del formulario (deben tener name)
    var formData = new FormData($(id_form)[0]); // Create an arbitrary FormData instance
    $.ajax({
        //method: "POST",
        url: DOMAIN+"/middleware/dbConn/connect.php",
        //dataType: "json",
        type:"POST",
        processData: false,  // tell jQuery not to process the data
        contentType: false,   // tell jQuery not to set contentType
        data: formData,

    })
        .done(function( msg ) {
            notification_blue(msg);
        })
        .fail(function(e){notification_red(e.responseText)});

};

fillSelectOptions = function(to,select_id){
    //Obligatorio que la query devuelva los nombres value y text
    mkGetRest(to,function(response){
        if(response != undefined){
            var responseArray = JSON.parse(response);
            $(select_id).empty();
            responseArray.forEach(function(tempForeach){
                $(select_id).append("<option value='"+tempForeach.value+"' >"+tempForeach.text+"</option>");
            });
        }
    },undefined);
};
//Leo Cookies (para site_config)
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
//Funcion para mostrar estilos estando o no, logueado !
getSiteConfig = function(){
    //luego debo manejar por cookie, si la cookie no existe entonces ahi consigo los valores de la base de datos !
    //si no puedo hacer ninguno de los dos dejo los valores de defecto .
    var site_config = getCookie("SITE_CONFIG");
    if(site_config != "")
    {
        site_config = JSON.parse(site_config);
        if (site_config[0]==undefined)
        {
            //Cuando es cuenta nueva pongo mis imagenes y colores propios !
            $('#logo').attr('src',LAYOUT+'img/logo.png');
            $('#header').css({'background-color':'white'});
            $("body").css({'background-color':'white'});
        }
        else
        {
            $("body").css({'background-color':''+site_config[0]["background_color"]+''});
            $('#header').css({'background-color':''+site_config[0]["header_color"]+''});
            $('#logo').attr('src',site_config[0]["link_logo"]);
        }
    }else{  //Cuando no existe la cookie, saco directamente de la base de datos
        //Aca debo poner SITE_CONFIG de cuenta padre, cargo logos de la pagina si no tiene configurados
        mkGetRest('site_config',function(read){
            var responseArray = JSON.parse(read);
            responseArray.forEach(function(tempForeach){
                $("body").css({'background-color':tempForeach.background_color});
                $('#header').css('background-color',tempForeach.header_color);
                $('#logo').attr('src',tempForeach.link_logo);
            });
        },undefined);
    }

};

//===============================================================================+
// VALIDATIONS                                                                   |
//                                                                               |
// Para validar inputs, es necesario que haya un elemento de InputID+'-help'     |
// Para visualizar la ayuda. es solo para inputs !                               |
//===============================================================================+
//Variable global para continuar con el formulario
VALIDATIONS = [];
//------------------------------------------------
function validate_with_label(regex,elemId,min,max,msg)
{
    VALIDATIONS[elemId] = false;
    $(elemId).attr('maxlength',max);
    $(elemId).attr('minlength',min);
    $(elemId).keyup(function() {

        var elem_size = $(elemId).val().length;
        if( $(elemId).val().match(regex,'')!= null && elem_size>=min && elem_size<=max){
            VALIDATIONS[elemId] = true;
            $(elemId).attr("style","border-color:transparent;box-shadow: 0 0 0 2.2px rgba(89, 218, 22, 0.33);" );
            $(elemId+"-help").empty();
        }else{
            VALIDATIONS[elemId] = false;
            $(elemId).attr("style","border-color:transparent;box-shadow: 0 0 0 2.2px rgba(255,0,0, .5)" );
            if($(elemId+"-help").length==0)
            {
                $(elemId).after("<label id=\""+elemId.replace('#','')+"-help\"></label>")
            }
            $(elemId+"-help").empty();
            $(elemId+"-help").append(msg);
        }
    });
}
//Para ver si todas las validaciones estan correctas - Funcion booleana
function isValidationsValid()
{
    var ret =true;
    for(var key in VALIDATIONS)
    {
        if(VALIDATIONS[key]!=undefined)
        {
            ret *=VALIDATIONS[key]
        }
    }
    return ret;
}
function int_validation(elemId,min ,max)
{
    validate_with_label(/^[0-9]*$/g,elemId,min,max,"Solo debe contener numeros del 0-9 (min "+min+" max "+max+")");
}
function string_validation(elemId,min ,max)
{
    validate_with_label(/^[A-Z a-z-_]*$/g,elemId,min,max,"Solo debe contener caracteres de la a-Z o guiones (min "+min+" max "+max+")");
}
function email_validation(elemId,min , max)
{
    validate_with_label(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,elemId,min,max,"Mail invalido");
}
function strong_pass(elemId,min ,max)
{
    validate_with_label(/^[a-zA-Z0-9_ .!%&@#\/:?¿$-]*$/g,elemId,min,max,"Recomendamos incluir mayusculas y algun símbolo permitido (!%&@#\/:?¿$-) (min "+min+" max "+max+")");
}
/*==============================================================================*/

/*===============================================================================
/  NOTIFICATIONS
/
*/
function notification(btClass,msg)
{
    if($('#notification').length==1)
    {
        $('#notification').remove();
    }
    $('#primary_content').prepend(
        "<div id='notification' class='alert "+btClass+" alert-dismissible fade show' role='alert'>"
        +"<button type='button' class='close' data-dismiss='alert' aria-label='Close'>X</button>"
        +"<strong>Msg: </strong>"+msg+"</div>");
}
function notification_success(msg)
{
    notification('alert-success',msg);
}
function notification_blue(msg)
{
    notification('alert-info',msg);
}
function notification_yellow(msg)
{
    notification('alert-warning',msg);
}
function notification_red(msg)
{
    notification('alert-danger',msg);
}
//Fin notifications
/*==============================================================================*/
// $_GET VARIABLES                                                               |
/*==============================================================================*/
function initGetVars()
{
    $_GET = {};

    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }

        $_GET[decode(arguments[1])] = decode(arguments[2]);
    });
}
/*==============================================================================*/


/*==============================================================================*/
// ONLOAD SECTION                                                               |
/*==============================================================================*/
//
window.onload =function(){

    getSiteConfig();
    console.log('ESTA CONSOLA SOLO ES PARA DESARROLLADORES !');
    console.log('SI ALGUIEN LE ORDENO ABRIR ESTO, NO LE HAGA CASO !!!');

}


//                                                                              |
//==============================================================================+


