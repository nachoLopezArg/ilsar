<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="id" dir="ltr">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />


    <!-- Title -->
    <title>Sorry, This Page Can&#39;t Be Accessed</title>
    <link href="<c:url value="/resources/public/css/http/403.css" />" rel="stylesheet"
          type="text/css">
</head>
<div class="lock"></div>
<div class="message">
    <h1>Access to this page is restricted</h1>
    <p>Please check with the site admin if you believe this is a mistake.</p>
</div>
</html>