<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" ></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <link href="<c:url value="/resources/public/css/bootstrap/4.0.0/bootstrap.min.css" />" rel="stylesheet" type="text/css">
    <link href="<c:url value="/resources/public/css/pages/login.css" />" rel="stylesheet" type="text/css">
    <script src="<c:url value="/resources/public/js/global.js" />"></script>


    <title>SDARBIL - Software Devolpment Argentina By Ignacio Lopez</title>
</head>

<body>
    <div class="col-md-12 col-xs-12" id="header">
        <div class="row">
            <div class="col-md-12  col-xs-12" id="logo_containter">
                <!--@logo-->
                <img id="logo" src="" alt="" style="width:25vh;height:auto;">
            </div>
        </div>
    </div>
    <!--start container-->
    <div class="containter">
        <div class="row" id="site_body">
            <div class="col-md-3">
                <!--@left_defult-->
            </div>
            <div class="col-md-6" id="primary_content">
                <!--@center_defult-->
                <form action="/login" method="POST">
                    <div class="lc-block">
                        <h4>Login</h4><hr>
                        <div class="row">
                            <div class="col-sm-12 col-xs-12 col-md-12  ">
                                            <span class="form-inline">
                                                <label class="option-name">Empresa</label>
                                                <input class="form-control" name="site_user" id="empresa" placeholder="ISHECMA INC." type="text">
                                            </span>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-12 ">
                                        <span class="form-inline">
                                            <label class="option-name">Usuario:</label>
                                            <input class="form-control" name="username" id="username" placeholder="Usuario" type="text">
                                        </span>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-12 ">
                                        <span class="form-inline">
                                            <label class="option-name">Contraseña:</label>
                                            <input class="form-control" name="password" id="password" placeholder="Contraseña" type="password">
                                        </span>
                                <hr>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-12 ">
                                            <span class="form-inline">
                                            <input class="form-control" value="Ingresar" type="submit">
                                            </span>
                            </div>
                            <div class="col-sm-12 col-xs-12 col-md-12" >
                                            <span class="form-inline">
                                                    <a href="#">Olvide mi contraseña</a>
                                            </span>
                            </div>

                        </div>
                        <c:if test="${param.error ne null}">
                            <div class="alert-danger">Invalid username and password.</div>
                        </c:if>
                        <c:if test="${param.logout ne null}">
                            <div class="alert-normal">You have been logged out.</div>
                        </c:if>
                    </div>
                </form>
            </div>
            <div class="col-md-3">
                <!--@rigth_defult-->
            </div>
        </div>
    </div>
    <!--end container-->

    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}" />

</body>
</html>
